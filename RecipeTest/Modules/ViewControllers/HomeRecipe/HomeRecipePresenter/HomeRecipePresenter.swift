//
//  HomeRecipePresenter.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation


protocol HomeRecipePresenterProtocol: NSObjectProtocol
{
    func showData(dataSource:[ModelCellProtocol])
    func fail()
}

class HomeRecipePresenter
{
    weak private var delegate: HomeRecipePresenterProtocol?
    private var connection: Connection?

    /**
     This function bind the delegate with the viewcontroller that conformas the delegate methods, recives
     - Parameter viewController: The viewcontroller that conforms HomeRecipePresenterDelegate methods
     */
    public func attachDelegate(viewController: HomeRecipePresenterProtocol)
    {
        self.delegate = viewController
    }
    
    /**
     This function makes the request in order to get a feed, or get the search recipe
     recives:
     - Parameter recipe: The recipe that will be searched
     - Parameter ingredients: The ingredients for the recipe
     */
    func getRecipes(recipe:String?,ingredientes: String?)
    {
        if self.connection == nil
        {
            self.connection = Connection()
            self.connection?.delegate = self
        }
        if let recipe = recipe, let ingredientes = ingredientes
        {

            self.connection?.getRequest(url: UrlFile.baseUrl+"i=\(ingredientes)&q=\(recipe)", dataType: ModelRecipe.self)
        }
        else
        {
            self.connection?.getRequest(url: UrlFile.baseUrl, dataType: ModelRecipe.self)
        }
    }
    
    /**
     This function receives the searchbar in order to get the recipe and the ingredients
     - Parameter searchBar: The searchBAr that will be use in order to get the recipe and ingredients
     */
    func getRecipeFromSearchBarText(searchBarText: String)
    {
        guard validateRecipe(text: searchBarText) else { return }
        let splitteString = searchBarText.components(separatedBy: ":")
        let recipeFor = splitteString[0]
        let ingredients = splitteString[1]
        getRecipes(recipe: recipeFor, ingredientes: ingredients)
    }
    
    func validateRecipe(text:String) -> Bool
    {
        guard text != "" else {return false}
        guard !text.isDigitNumber() else {return false}
        if text.countInstances(of: ":") > 1 ||  text.countInstances(of: ":") == 0{return false}
        
        return true
    }

}


//MARK: - ConnectionDelegate methods
extension HomeRecipePresenter: ConnectionDelegate
{
    func objectsReceived<T>(objects: [T]?, connection: Connection?) where T : Decodable, T : Encodable {
        //
    }
    
    func objectReceived<T>(object: T?, connection: Connection?) where T : Decodable, T : Encodable {
        guard let recipes = object as? ModelRecipe else
        {
            self.delegate?.fail()
            return
        }


        self.delegate?.showData(dataSource: recipes.results)
    }
    
    func failedRequest(connection: Connection) {
        self.delegate?.fail()
    }
}


extension String {
    func countInstances(of stringToFind: String) -> Int {
        var stringToSearch = self
        var count = 0
        while let foundRange = stringToSearch.range(of: stringToFind, options: .diacriticInsensitive) {
            stringToSearch = stringToSearch.replacingCharacters(in: foundRange, with: "")
            count += 1
        }
        return count
    }
}
