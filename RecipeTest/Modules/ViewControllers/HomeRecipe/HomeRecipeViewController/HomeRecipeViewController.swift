//
//  HomeRecipeViewController.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import UIKit

class HomeRecipeViewController: UIViewController, Storyboarded
{
    private var presenter = HomeRecipePresenter()
    private var dataSource = [ModelCellProtocol]()
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachDelegate(viewController: self)
        intiTableView()
        makeRequest()
        // Do any additional setup after loading the view.
    }

}


//MARK: - private methods
extension HomeRecipeViewController
{
    func intiTableView()
    {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.register(HomeRecipeViewCell.nib, forCellReuseIdentifier: HomeRecipeViewCell.identifier)
    }
    
    /**
     This function makes the request
     */
    func makeRequest()
    {
        self.presenter.getRecipes(recipe: nil, ingredientes: nil)
    }
}


//MARK: - UITableView delegate & dataSource methods
extension HomeRecipeViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeRecipeViewCell.identifier, for: indexPath) as? HomeRecipeViewCell else { return UITableViewCell() }
        cell.configure(model: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailRecipeViewController.instantiate()
        vc.detailRecipeModel = dataSource[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: - HomeRecipePresenterProtocol methods
extension HomeRecipeViewController: HomeRecipePresenterProtocol
{
    func showData(dataSource: [ModelCellProtocol]) {
        OperationQueue.main.addOperation {
            self.dataSource = dataSource
        }
    }

    func fail() {
        print("show")
    }
}
