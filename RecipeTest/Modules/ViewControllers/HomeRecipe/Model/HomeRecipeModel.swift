//
//  HomeRecipeModel.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation

class HomeRecipeModel: Codable, ModelCellProtocol
{
    var title: String
    
    var href: String
    
    var ingredients: String
    
    var thumbnail: String?
    
    /**
     
     Just used if we want to change the name of the attribute in orer to usen it
     instead of the original parameter name
     */
    
    enum CodingKeys: String,CodingKey
    {
        case title
        case href
        case ingredients
        case thumbnail
    }
    
    init(title: String, href: String,ingredients: String, thumbnail: String)
    {
        self.title = title
        self.href = href
        self.ingredients = ingredients
        self.thumbnail = thumbnail
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        href = try values.decode(String.self, forKey: .href)
        ingredients = try values.decode(String.self, forKey: .ingredients)
        thumbnail = try values.decode(String.self, forKey: .thumbnail)
    }
}
