//
//  HomeRecipeViewCell.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import UIKit



class HomeRecipeViewCell: UITableViewCell {

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageThumbnail: UIImageView!
    let multimediaDownloader = MultimediaDownloaderController()
    var moreRecentUrlString: String = ""


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setCornerRaadius()
    }
    
    static var identifier:String
    {
        return String(describing: self)
    }
    
    static var nib: UINib
    {
        let bundle = Bundle.init(for: self)
        return UINib(nibName: identifier, bundle: bundle)
    }

    func configure(model : ModelCellProtocol)
    {
        moreRecentUrlString     = model.thumbnail ?? ""
        self.labelTitle.text    = model.title
        setBackGroundCellColor(ingredients: model.ingredients.components(separatedBy: ","))
        guard let imageUrlForCell = model.thumbnail else {return}
        if imageUrlForCell != ""
        {
            downloadOrLoadImage(imageUrlForCell: imageUrlForCell,moreRecentUrlString: moreRecentUrlString)
        }
        else
        {
            self.imageThumbnail.image = UIImage.init(named: "iconImage")
        }
        
    }
    
    /**
     This function set the backgroundcolor for the cell in funciton of the first ocurrenc of an ingredient in our array of ingredients IngredientsFile
     */
    func setBackGroundCellColor(ingredients: [String])
    {
        print(ingredients)
        for ingredient in Ingredients.arrayOfIngredients
        {
            if ingredients.contains(ingredient)
            {
                if ingredient == "onions"
                {
                    self.customView.backgroundColor = UIColor.init(red: 179.0/255.0, green: 151.0/255.0, blue: 24.0/255.0, alpha: 1.0)
                }
                if ingredient == "tomato"
                {
                    self.customView.backgroundColor = UIColor.init(red: 179.0/255.0, green: 57.0/255.0, blue: 24.0/255.0, alpha: 1.0)
                }
                if ingredient == "cilantro"
                {
                    self.customView.backgroundColor = UIColor.init(red: 27.0/255.0, green: 158.0/255.0, blue: 116.0/255.0, alpha: 1.0)

                }
                if ingredient == "garlic"
                {
                    self.customView.backgroundColor = UIColor.init(red: 24.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0)
                }
                break
            }
            else
            {
                self.customView.backgroundColor = UIColor.init(red: 117.0/255.0, green: 138.0/255.0, blue: 164.0/255.0, alpha: 1.0)
            }
        }
    }
    
    /**
     This function dowload or load the image form service and cache respective
     receives:
     - Parameter urlString: The url string of the thumbnail
     */
    private func downloadOrLoadImage(imageUrlForCell:String,moreRecentUrlString:String)
    {
        multimediaDownloader.downloadImage(urlString: imageUrlForCell)
        {
            [weak self]
            (result) in
            guard let self = self else {return}
            guard self.moreRecentUrlString == moreRecentUrlString else {return}
            switch result {
            case .onSucces(let image):
                self.imageThumbnail.image = image
            default:
                break
            }
        }
    }
    
    private func setCornerRaadius()
    {
        imageThumbnail.layer.cornerRadius = 5.0
        imageThumbnail.layer.masksToBounds = true
        
        customView.layer.cornerRadius = 10.0
        customView.layer.masksToBounds = true
    }
}
