//
//  SearchRecipeViewController.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SearchRecipeViewController: UIViewController, Storyboarded
{

    @IBOutlet weak var tableView: UITableView!
    private var isSearching: Bool = false
    private var presenter = HomeRecipePresenter()
    private var dataSource = [ModelCellProtocol]()
    {
        didSet
        {
            self.tableView.reloadData()
        }
    }
    private var filteredDataSource: [ModelCellProtocol] = []

    @IBOutlet weak var searchBar: UISearchBar!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        self.presenter.attachDelegate(viewController: self)
        initUI()
        intiTableView()
        makeRequest()
        // Do any additional setup after loading the view.
    }
    
}

//MARK: - private methods
extension SearchRecipeViewController
{
    func intiTableView()
    {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.separatorStyle = .none
        self.tableView.register(HomeRecipeViewCell.nib, forCellReuseIdentifier: HomeRecipeViewCell.identifier)
    }
    
    /**
     This function makes the request for home feed, recomendations
     */
    func makeRequest()
    {
        self.presenter.getRecipes(recipe: "omelete", ingredientes: "tomato,onions")
    }
    
    /**
     initialize other UI Views
     */
    func initUI()
    {
        searchBar.delegate = self
    }
}


//MARK: - UITaleViewDelegate and datasource methods
extension SearchRecipeViewController:UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeRecipeViewCell.identifier, for: indexPath) as? HomeRecipeViewCell else {return UITableViewCell()}
        cell.configure(model: dataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailRecipeViewController.instantiate()
        vc.detailRecipeModel = dataSource[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - SearchBar delegate methods
extension SearchRecipeViewController: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        guard text.isEmpty == false else {return true}
        guard let textB = searchBar.text else {return true}
        guard !textB.isDigitNumber() else {return false}
        if textB.countInstances(of: ":") > 1{return false}
        return true
    }
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        self.presenter.getRecipeFromSearchBarText(searchBarText: searchBar.text ?? "")
    }
}


//MARK: - HomeRecipePresenterProtocol methods
extension SearchRecipeViewController: HomeRecipePresenterProtocol
{
    func showData(dataSource: [ModelCellProtocol]) {
        OperationQueue.main.addOperation {
            self.dataSource = dataSource
        }
    }
    
    func fail() {
        print("show alert with error")
    }
}




