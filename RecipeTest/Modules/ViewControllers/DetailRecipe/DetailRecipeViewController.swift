//
//  DetailRecipeViewController.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import UIKit

class DetailRecipeViewController: UIViewController,Storyboarded {

    @IBOutlet weak var principalImageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelIngredients: UILabel!
    public var detailRecipeModel: ModelCellProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadDetailData()
    }

    @IBAction func loadWebView(_ sender: Any) {
        sendToWebView()
    }
    
}

//MARK: - load UI
extension DetailRecipeViewController
{
    private func loadDetailData()
    {
        guard let modelData = detailRecipeModel else {return}
        chekForImage(urlString: modelData.thumbnail ?? "")
        self.labelTitle.text        = modelData.title
        self.labelIngredients.text  = modelData.ingredients
    }
    
    private func chekForImage(urlString:String)
    {
        let multimediaDownloader = MultimediaDownloaderController()
        multimediaDownloader.downloadImage(urlString: urlString )
        {
            [weak self]
            (result) in
            guard let self = self else {return}
            switch result {
            case .onSucces(let image):
                self.principalImageView.image = image
            default:
                break
            }
        }
    }
    
    private func sendToWebView()
    {
        if detailRecipeModel?.href != ""
        {
            let wvc = WKWebViewController.instantiate()
            wvc.url = detailRecipeModel?.href
            self.navigationController?.pushViewController(wvc, animated: true)
        }
    }
}
