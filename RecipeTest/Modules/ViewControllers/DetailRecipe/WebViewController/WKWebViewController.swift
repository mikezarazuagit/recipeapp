//
//  WKWebViewController.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/18/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import UIKit
import WebKit

class WKWebViewController: UIViewController,Storyboarded
{

    @IBOutlet weak var wkWebView: WKWebView!
    public var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let loadUrl = self.url else {return}
        wkWebView.load(URLRequest(url: URL(string:loadUrl)!))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
