//
//  UrlsFile.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation


struct UrlFile
{
    static let baseUrl: String = "http://www.recipepuppy.com/api/?"
    static let homeUrl: String = baseUrl + "i=onions,garlic,cream&q=pasta&p=3"
}

