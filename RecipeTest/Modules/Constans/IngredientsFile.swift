//
//  IngredientsFile.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/18/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation

/**
 We define here the respective color for each ingredient in the arrayOfIngredients,
 where
 
 "onions"   = UIColor.init(red: 179.0/255.0, green: 151.0/255.0, blue: 24.0/255.0, alpha: 1.0)
 "tomato"   = UIColor.init(red: 179.0/255.0, green: 57.0/255.0, blue: 24.0/255.0, alpha: 1.0)
 "cilantro" =UIColor.init(red: 27.0/255.0, green: 158.0/255.0, blue: 116.0/255.0, alpha: 1.0)
 "garlic"   = UIColor.init(red: 24.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0)
 
 And any other ingredient will be painted with color = UIColor.init(red: 24.0/255.0, green: 95.0/255.0, blue: 179.0/255.0, alpha: 1.0)
 */

struct Ingredients
{
    static let arrayOfIngredients = ["onions","garlic","cilantro","tomato"]
}
