//
//  Connection.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation
import Alamofire

protocol ConnectionDelegate:class
{
    /**
     This function is delegated by the class that confroms ConnectionDelegate methods, returns the objects array from a services already mapped by Codable protocol
     */
    func objectsReceived<T : Codable>(objects: [T]?, connection: Connection?)
    /**
     This function is delegated by the class that confroms ConnectionDelegate methods, returns the objects array from a services already mapped by Codable protocol
     */
    func objectReceived<T : Codable>(object: T?, connection: Connection?)
    /**
     This function is delegated by the class that conforms ConnectionDelegate methods, it is called when the request fail
     */
    func failedRequest(connection : Connection)
}


class Connection
{
    weak var delegate: ConnectionDelegate?
    
    /**
     This function recives the url from get the data and the respective array of objects type T where T conforms Codable protocol
     - Parameter url: The string value of the url to get data
     - Parameter dataType: The array of objects that conforms Codable protocol
     */
    public func getRequest<T: Codable>(url: String, dataType: [T].Type)
    {
        //Where headers are if we need a authorization token or other ype of data
        var headers = [String:String]()

        //Manage the request and his timeout
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers:headers).responseJSON
            {
                (response) in
                switch(response.result)
                {
                case .success(_):
                    if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! <= 299
                    {
                        do
                        {
                            let device = try JSONDecoder().decode([T].self, from: response.data!)
                            print("Succes objectsReceived")
                            self.delegate?.objectsReceived(objects: device, connection: self)
                        }
                        catch let error
                        {
                            print(error)
                            let v : [T]? = nil
                            self.delegate?.objectsReceived(objects: v, connection: self)
                        }
                        
                    }
                    else
                    {
                        self.delegate?.failedRequest(connection: self)
                    }
                    break
                    
                case .failure(let error):
                    self.delegate?.failedRequest(connection: self)
                    debugPrint("getEvents error: \(error)")
                    break
                }
                
            }.resume()
    }
    
    /**
     This function recives the url from get the data and the respective array of objects type T where T conforms Codable protocol
     - Parameter url: The string value of the url to get data
     - Parameter dataType: The object that conforms Codable protocol
     */
    public func getRequest<T:Codable>(url: String, dataType: T.Type)
    {
        //Where headers are if we need a authorization token or other ype of data
        var headers = [String:String]()
        //        if(!UserDefaults.standard.bool(forKey: "isLogged")){return}
        //
        //        if let currentToken = UserDefaults.standard.string(forKey: "userToken")
        //        {
        //            headers = ["Authorization":"Bearer \(currentToken)"]
        //        }
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers:headers).responseJSON
            {
                
                (response) in
                switch(response.result)
                {
                case .success(_):
                    if (response.response?.statusCode)! >= 200 && (response.response?.statusCode)! <= 299
                    {
                        do
                        {
                            let device = try JSONDecoder().decode(T.self, from: response.data!)
                            
                            self.delegate?.objectReceived(object: device, connection: self)
                        }
                        catch let error
                        {
                            print(error)
                            let device : T? = nil
                            
                            self.delegate?.objectReceived(object: device, connection: self)
                            
                        }
                        
                    }
                    else
                    {
                        let device : T? = nil
                        
                        //print(response.result.value)
                        self.delegate?.objectReceived(object: device, connection: self)
                    }
                    break
                    
                case .failure(let error):
                    self.delegate?.failedRequest(connection: self)
                    debugPrint("getEvents error: \(error)")
                    break
                }
                
            }.resume()
        
    }
}
