//
//  ModelRecipe.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation

class ModelRecipe: Codable
{
    var title: String = ""
    var version: Double = 0.0
    var href: String = ""
    var results: [HomeRecipeModel] = [HomeRecipeModel]()
    
    init(title:String,version:Double,href:String,results:[HomeRecipeModel]) {
        self.title = title
        self.version = version
        self.href = href
        self.results = results
    }
    
    private enum CodingKeys: String,CodingKey
    {
        case title
        case version
        case href
        case results
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        version = try values.decode(Double.self, forKey: .version)
        href = try values.decode(String.self, forKey: .href)
        results = try values.decode([HomeRecipeModel].self, forKey: .results)
    }
}
