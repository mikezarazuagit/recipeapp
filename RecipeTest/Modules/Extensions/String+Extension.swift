//
//  String+Extension.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/18/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation

extension String
{
    
    func isDigitNumber() -> Bool
    {
        guard self.count == 1 else {return false}
        let numberExp = "[0-9]"
        let numberTest = NSPredicate(format: "SELF MATCHES %@", numberExp)
        return numberTest.evaluate(with: self)
    }
    
}
