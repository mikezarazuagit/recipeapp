//
//  MultimediaDownloaderController.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/17/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation
import Alamofire


/**
 This enum is used in order to detmerinate the state of a download request
 */
enum ImageDownloadResult
{
    
    case noInternetConnection
    case error(message: String)
    case onSucces(UIImage)
    
}


class MultimediaDownloaderController
{
    static let imageCache = MultimediaImageCache()
    
    
    // Property that Determinates if the network is reacheable
    private var networtkIsReachable: Bool
    {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    /**
     This funtion downloads a simple image and returns it in the completion, if the image
     has been already downloaded before then return the image related to the url key
     Receives:
     - Parameter urlString: The url in String
     - Parameter completion: A escaping closure that receives a ImageDownloadResult
     Return:
     -A uiimage if is success, or error if fails
     **/
    func downloadImage(urlString: String, completion: @escaping (ImageDownloadResult) -> ())
    {
        
        if let image =  MultimediaDownloaderController.imageCache.getFromCache(url: urlString)
        {
            DispatchQueue.main.async { completion(.onSucces(image)) }
            return
        }
        
        guard networtkIsReachable else
        {
            DispatchQueue.main.async { completion(.noInternetConnection) }
            return
        }
        guard !urlString.isEmpty, let url = URL(string: urlString) else
        {
            DispatchQueue.main.async { completion(.error(message: "URL de multimedia inválido")) }
            return
        }
        Alamofire.request(url).responseData
        {
            (data) in
            switch data.result
            {
            case .failure(let error):
                //if fails then retunr an error in the completion
                DispatchQueue.main.async
                    {
                        completion(.error(message: error.localizedDescription))
                    }
            case .success(let data):
                //if success then we try to create an image from the data received, store in cache and then  send the image in the completion
                guard let image = UIImage(data: data) else
                {
                    DispatchQueue.main.async { completion(.error(message: "No se descargó ninguna imagen")) }
                    return
                }
                MultimediaDownloaderController.imageCache.setOnCache(name: urlString, image: image)
                DispatchQueue.main.async { completion(.onSucces(image)) }
            }
        }
    }

}
