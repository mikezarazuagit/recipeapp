//
//  MultimediaImageCache.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/17/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation
import UIKit

/**
 This class stores images in a NSCache instance
 **/
class MultimediaImageCache {
    
    /**
     A instance of image cache
     **/
    private let imageCache = NSCache<AnyObject, AnyObject>()
    
    /**
     This function get a uiimage from the cache
     - Parameter url: Is the url of the image
     - Returns: The image from the cache, or nil if not exists
     **/
    func getFromCache(url: String) -> UIImage? {
        return imageCache.object(forKey: url as AnyObject) as? UIImage
    }
    
    /**
     This function saves in cache a uiimage
     - Parameter url: Is the url of the image
     - Returns: Void
     **/
    func setOnCache(name: String, image: UIImage) {
        imageCache.setObject(image, forKey: name as AnyObject)
    }
    
    /**
     This function clears the current cache
     **/
    func clearCache() {
        
        imageCache.removeAllObjects()
    }
    
}
