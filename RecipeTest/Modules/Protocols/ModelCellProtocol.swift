//
//  ModelCellProtocol.swift
//  RecipeTest
//
//  Created by Mike Zarazua on 7/16/19.
//  Copyright © 2019 Mike Zarazua. All rights reserved.
//

import Foundation

protocol ModelCellProtocol
{
    var title: String {get set}
    var href: String {get set}
    var ingredients: String {get set}
    var thumbnail: String? {get set}
}


